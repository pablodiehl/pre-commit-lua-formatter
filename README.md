# pre-commit-lua-formatter

pre-commit hook to format Lua files using LuaFormatter.

# Usage
This hook is meant to be used with [pre-commit](https://pre-commit.com/).

Add the following hook to `.pre-commit-config.yaml`:

```yaml
  - repo: https://gitlab.com/pablodiehl/pre-commit-lua-formatter.git
    rev: v1.0.0
    hooks:
      - id: lua-formatter
```

# Dependencies
This hook depends on [LuaFormatter](https://github.com/LuaDevelopmentTools/luaformatter), you can install it using [LuaRocks](https://luarocks.org/modules/luarocks/formatter):
```shell
$ luarocks install formatter
```
